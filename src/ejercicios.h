#ifndef ETPH_EJERCICIOS_H
#define ETPH_EJERCICIOS_H

#include <iostream>
#include <vector>
#include <math.h>
#include <tuple>
#include "definiciones.h"

using namespace std;

#include "ejercicios.h"


/*-------------------------- PROCEDIMIENTO 1 -------------------------- */
bool esEncuestaValida(eph t);

//funciones para ENCUESTA VALIDA
bool esMatriz(eph t);
bool individuosValidos(eph t);
bool individuoValido(individuo p);
bool individuosDistintos(eph t);
bool mismoAno(eph t);
bool hogaresCoherentes (eph t);
bool mismoHogar (individuo p1, individuo p2);
bool mismoITF (individuo p1, individuo p2);
bool mismaCantidadMiembros (individuo p1, individuo p2);
bool mismaCantidadAdultos (individuo p1, individuo p2);
bool noTrabajaNoTieneHoras(individuo p);
bool noTrabajaNoTienePago(individuo p);

/*-------------------------- PROCEDIMIENTO 2 --------------------------*/
int laMejorEdad(eph t);

//funciones de LA MEJOR EDAD
int cantidadEdad (eph t, int edad);
int sumaIngresoXEdad (eph t, int edad);
int promedioIngresoXEdad (eph t, int edad);


/*-------------------------- PROCEDIMIENTO 3 --------------------------*/
float promedioIngresoProfesional(eph t);

//funciones para PROMEDIO INGRESO PROFESIONAL
int ingresosPromedioProfXHora (eph t);
bool esProfesionalConIngresos (individuo p);
int cantProfesionales (eph t);
bool esProfesional (individuo p);
bool declaroIngresosYHoras (individuo p);


/*-------------------------- PROCEDIMIENTO 4 --------------------------*/
int hogarDeMayorIngreso(eph t);

// Funciones para HOGAR DE MAYOR INGRESO
bool esHogar (eph t, int c);
bool esMayorIngreso (eph t, int cod, int result);
int ingresoHogar (eph t, int cod);
bool igualIngresoMenorIndice (eph t, int cod, int result);
int indiceCodusu (eph t, int cod);
bool esElDeMayorIngreso (eph t, int result);


/*-------------------------- PROCEDIMIENTO 5 --------------------------*/
individuo mejorNoProfesional(eph t);

// Funciones de MEJOR NO PROFESIONAL
bool esNoProfesionalCIMAPP (eph t, individuo p);


/*-------------------------- PROCEDIMIENTO 6 --------------------------*/
bool sigoEstudiando(eph t);

// Funciones de sigoEstudiando
int desocupadosNoUniversitarios (eph t);
int desocupadosUniversitarios (eph t);


/*-------------------------- PROCEDIMIENTO 7 --------------------------*/
individuo empleadoDelAnio(eph t);

// Funciones de empleadoDelAño
bool esEmpleadoDelAno (eph t, individuo p);
bool existePatronQueGanaMenos (eph t, individuo p);


/*-------------------------- PROCEDIMIENTO 8 --------------------------*/
bool noTieneAQuienCuidar(eph t);


/*-------------------------- PROCEDIMIENTO 9 --------------------------*/
bool pareto(eph t);

//funciones para PARETO
int sumaIngresos (eph p, int desde, int hasta);
void ordenarPorP47T(eph &t);


/*-------------------------- PROCEDIMIENTO 10 --------------------------*/
int elDeMayorIncrementoInterAnual(eph t1, eph t2);

// Funciones de elDeMayorIncrementoInterAnual
bool columnasIgual (individuo p1, individuo p2);
bool columnasDistintas (individuo p1, individuo p2);
bool esElDeMayorIncremento (eph t1, eph t2, individuo p1, individuo p2);
bool seMantiene (eph t1, eph t2, individuo p1, individuo p2);
bool esParecido (individuo p1, individuo p2);
bool mismaActividad (individuo p1, individuo p2);
bool pasanLosAnios (individuo p1, individuo p2);


/*-------------------------- PROCEDIMIENTO 11 --------------------------*/
vector<tuple<int,float> > mejorQueLaInflacion(eph t1, eph t2, float infl);

//FUNCIONES PARA MEJORQUELAINFLACION
float inflacion(eph t1, eph t2, int c);
float promedioPaga(eph t, int c);
int cantCodOcup(eph t, int c);
bool estaTupla (vector<tuple <int, float> > t, int c);
void ordenarPorInflacion(vector<tuple<int,float> > &t);


/*-------------------------- PROCEDIMIENTO 12 --------------------------*/
void ordenar(eph &t);

//FUNCIONES PARA ORDENAR POR CODUSU Y EDAD
void ordenarCODUSU(eph &t);
void ordenarEDADxCODUSU(eph &t);


/*-------------------------- PROCEDIMIENTO 13 --------------------------*/
void agregarOrdenado(eph &t, individuo ind);

//FUNCIONES PARA AGREGAR ORDENADO
bool esta(eph t, individuo ind);


/*-------------------------- PROCEDIMIENTO 14 --------------------------*/
void quitar(eph &t, individuo ind);

//FUNCIONES PARA QUITAR
eph eliminar (eph t, individuo ind);
int sumaCODUSU(eph t, int n);


#endif //ETPH_EJERCICIOS_H
