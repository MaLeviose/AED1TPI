#include "../Funciones_TPI.h"
#include "../ejercicios.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(encuestaValidaTEST, esValida){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          0,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(true, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, noESMatriz){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1, 3},
            {37,    2018, 11,          0,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, codusuFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {-5,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          0,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, componenteFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, -3,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          0,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, ano4FueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          0,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    -14,  12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, nivelEdFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          3,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, estadoFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,       20,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, catOcupFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,       0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      -4,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      24,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, edadFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        112,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        -34,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,         72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, pp3e_TotFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   -5,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, itfFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     -4, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, p21FueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,       -3,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, p47FueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    -5,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, pp04d_CodFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       -5,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, cat_InacFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          9},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, ix_Mayeq10FueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      0,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, ix_TotFueraRango){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 0,      0,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, adultosMenoresATotal){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      5,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, desempleadoConSueldo){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,      500,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, desempleadoQueTrabaja){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   30,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, desempleadoOcupado){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       30,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, distintoAno){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2016, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}

TEST(encuestaValidaTEST, individuosIguales){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 11,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}


TEST(encuestaValidaTEST, hogaresDistintoITF){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     20, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}



TEST(encuestaValidaTEST, hogaresDistintaIX_Tot){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 3,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      2,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}


TEST(encuestaValidaTEST, hogaresDistintaIX_MAYEQ10){
    eph t1 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21,   P47T,  ITF,   IX_TOT, IX_MAYEQ10, CAT_INAC},
            {36,    2018, 11,          0,        1,      0,        30,   20,       30,       21,    50,     34, 4,      4,          1},
            {37,    2018, 11,          1,        2,      3,        16,   -1,       -1,        0,   200,     15, 2,      2,          6},
            {37,    2018, 12,          1,        1,      4,        72,   44,       50,      200,   200,     15, 2,      1,          7},
    };
    EXPECT_EQ(false, esEncuestaValida(t1));
}
