#include "../definiciones.h"
#include "../Funciones_TPI.h"
#include "../ejercicios.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <tuple>

using namespace std;

TEST(mejorQueLaInflacion, prueba1) {
    float inf = 20.0;
    lista_exitosos ts = {make_tuple(20301, 30.0), make_tuple(40311, 25.0)};
    lista_exitosos ts_out;

    eph t1 = {
            {1,  2016, 1, 0, 1, 0, 56, -1, -1,    0,     0,     0,     1, 1, 0},
            {2,  2016, 1, 0, 0, 0, 17, -1, -1,    0,     0,     0,     1, 1, 3},
            {4,  2016, 1, 0, 0, 0, 20, -1, -1,    0,     0,     0,     1, 1, 3},
            {3,  2016, 1, 1, 1, 0, 35, 30, 40311, 33000, 33000, 33000, 1, 1, 0},
            {5,  2016, 1, 1, 3, 0, 20, 40, -1,    20000, 20000, 20000, 1, 1, 0},
            {10, 2016, 1, 1, 3, 0, 35, 30, 20301, 25000, 25000, 25000, 1, 1, 0},
    };

    eph t2 = {
            //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21, P47T, ITF, IX_TOT, IX_MAYEQ10, CAT_INAC};
            {1,  2017, 1, 0, 1, 0, 57, -1, -1,    0,     0,     0,     1, 1, 0},
            {2,  2017, 1, 0, 0, 0, 18, -1, -1,    0,     0,     0,     1, 1, 3},
            {4,  2017, 1, 0, 0, 0, 21, -1, -1,    0,     0,     0,     1, 1, 3},
            {3,  2017, 1, 1, 1, 0, 36, 30, 40311, 41250, 41250, 41250, 1, 1, 0},
            {5,  2017, 1, 1, 3, 0, 21, 40, -1,    20000, 21000, 21000, 1, 1, 0},
            {10, 2017, 1, 1, 3, 0, 36, 30, 20301, 32500, 32500, 32500, 1, 1, 0},
    };

    ts_out = mejorQueLaInflacion(t1, t2, inf);
    cout << "Encontradas " << ts_out.size() << " actividades" << endl;
    ASSERT_EQ(ts.size(), ts_out.size());
    for (int i = 0; i < ts.size(); i++) {
        paritaria_exitosa t1 = ts[i];
        paritaria_exitosa t2 = ts_out[i];

        // test codigos de actividad
        int m1 = get<0>(t1);
        int m2 = get<0>(t2);
        EXPECT_EQ(m1, m2);

        // test incremento ingreso
        float i1 = round(get<1>(t1));
        float i2 = round(get<1>(t2));
        EXPECT_EQ(i1, i2);

    }
}

    TEST(mejorQueLaInflacion, PP04CODIgualesEnTuplas) {
        float inf = 20.0;
        lista_exitosos ts = {make_tuple(20301,30.0),make_tuple(40311,25.0)} ;
        lista_exitosos ts_out;

        eph t1 = {
                {1,  2016, 1, 0, 1, 0, 56, -1, -1,    0,     0,     0,      1, 1, 0},
                {2,  2016, 1, 0, 0, 0, 17, -1, -1,    0,     0,     0,      1, 1, 3},
                {4,  2016, 1, 0, 0, 0, 20, -1, -1,    0,     0,     0,      1, 1, 3},
                {3,  2016, 1, 1, 1, 0, 35, 30, 40311, 33000, 33000, 33000,  1, 1, 0},
                {5,  2016, 1, 1, 3, 0, 20, 40, -1,    20000, 20000, 20000,  1, 1, 0},
                {10, 2016, 1, 1, 3, 0, 35, 30, 20301, 25000, 25000, 25000,  1, 1, 0},
                {11, 2016, 1, 1, 1, 0, 35, 30, 40311, 33000, 33000, 33000,  1, 1, 0},
        };

        eph t2 = {
                //{CODUSU, ANO4, COMPONENTE, NIVEL_ED, ESTADO, CAT_OCUP, EDAD, PP3E_TOT, PP04D_COD, P21, P47T, ITF, IX_TOT, IX_MAYEQ10, CAT_INAC};
                {1,  2017, 1, 0, 1, 0, 57, -1, -1,    0,     0,     0,      1, 1, 0},
                {2,  2017, 1, 0, 0, 0, 18, -1, -1,    0,     0,     0,      1, 1, 3},
                {4,  2017, 1, 0, 0, 0, 21, -1, -1,    0,     0,     0,      1, 1, 3},
                {3,  2017, 1, 1, 1, 0, 36, 30, 40311, 41250, 41250, 41250,  1, 1, 0},
                {5,  2017, 1, 1, 3, 0, 21, 40, -1,    20000, 21000, 21000,  1, 1, 0},
                {10, 2017, 1, 1, 3, 0, 36, 30, 20301, 32500, 32500, 32500,  1, 1, 0},
                {11, 2017, 1, 1, 1, 0, 36, 30, 40311, 41250, 41250, 41250,  1, 1, 0},
        };

        ts_out = mejorQueLaInflacion(t1,t2,inf);
        cout << "Encontradas " << ts_out.size() << " actividades" << endl;
        ASSERT_EQ(ts.size(),ts_out.size());
        for(int i=0;i<ts.size();i++){
            paritaria_exitosa t1 = ts[i];
            paritaria_exitosa t2 = ts_out[i];

            // test codigos de actividad
            int m1 = get<0>(t1);
            int m2 = get<0>(t2);
            EXPECT_EQ(m1,m2);

            // test incremento ingreso
            float i1 = round(get<1>(t1));
            float i2 = round(get<1>(t2));
            EXPECT_EQ(i1,i2);

        }

    }



