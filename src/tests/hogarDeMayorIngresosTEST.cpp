#include "../Funciones_TPI.h"
#include "../ejercicios.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(hogarDeMayorIngresos, prueba) {

    eph t1 = leerEncuesta("datos/eth_01.csv");

    EXPECT_EQ(17, hogarDeMayorIngreso(t1));
}