#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include <sstream>

#include "Funciones_TPI.h"

using namespace std;

void grabarEncuesta(eph t, string nombreArchivo) {
    ofstream miArchivo;
    individuo ind;
    miArchivo.open(nombreArchivo.c_str());
    if(miArchivo.is_open()) {
        for(int i=0;i<t.size();i++) {
            ind = t[i];
            miArchivo << ind[0];
            for(int j=1;j<ind.size();j++) {
                miArchivo << "," << ind[j];
            }
            miArchivo << endl;
        }
    }
    else{
        cout << "Error grabando el archivo." << endl;
    }
    miArchivo.close();
}

eph leerEncuesta(string nombreArchivo) {
    ifstream miArchivo;
    eph t;
    string line;

    miArchivo.open(nombreArchivo.c_str(), ifstream::in);
    if(miArchivo.is_open()) {
        while (getline(miArchivo, line)) {
            individuo ind;
            std::istringstream linestream(line);
            std::string value;

            while (getline(linestream, value, ',')) {
                ind.push_back(stoi(value));
            }
            t.push_back(ind);
        }
        miArchivo.close();
    }
    else
        cout << "No se pudo abrir el archivo " << nombreArchivo << endl;

    return t;
}


