#ifndef UTILES_FUNCIONES_TPI_H
#define UTILES_FUNCIONES_TPI_H

#include <iostream>
#include <vector>
#include <fstream>
#include "definiciones.h"

using namespace std;

void grabarEncuesta(eph t, string nombreArchivo);
eph leerEncuesta(string nombreArchivo);

#endif //UTILES_FUNCIONES_TPI_H
